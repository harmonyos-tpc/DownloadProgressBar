/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.panwrona.downloadprogressbar.slice;

import com.panwrona.downloadprogressbar.ResourceTable;
import com.panwrona.downloadprogressbar.library.DownloadProgressBar;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

public class ManuaProgressSlice extends AbilitySlice {
    private int val = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);


        final DownloadProgressBar downloadProgressView = (DownloadProgressBar)findComponentById(ResourceTable.Id_dpv3);
        final Text successTextView = (Text)findComponentById(ResourceTable.Id_success_text_view);

        successTextView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                val = val + 10;
                downloadProgressView.setProgress(val);
            }
        });

        downloadProgressView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                downloadProgressView.playManualProgressAnimation();
            }
        });
        downloadProgressView.setOnProgressUpdateListener(new DownloadProgressBar.OnProgressUpdateListener() {
            @Override
            public void onProgressUpdate(float currentPlayTime) {

                successTextView.setText( Math.round(currentPlayTime / 3.6) + " %");
            }

            @Override
            public void onStarted() {
                downloadProgressView.setEnabled(false);
            }

            @Override
            public void onEnded() {
                val = 0;
                successTextView.setText("Click to download");
                downloadProgressView.setEnabled(true);
            }

            @Override
            public void onAnimationSuccess() {

                successTextView.setText("Downloaded!");
            }

            @Override
            public void onAnimationError() {
                successTextView.setText("Aborted!");
            }

            @Override
            public void onManualProgressStarted() {

            }

            @Override
            public void onManualProgressEnded() {

            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
