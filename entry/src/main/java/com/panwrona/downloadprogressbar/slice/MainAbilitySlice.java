/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.panwrona.downloadprogressbar.slice;

import com.panwrona.downloadprogressbar.ResourceTable;
import com.panwrona.downloadprogressbar.library.DownloadProgressBar;
import com.panwrona.downloadprogressbar.library.LogUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

public class MainAbilitySlice extends AbilitySlice {
    private Button button, button2;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_list_ability_main, null, false);

        button = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_1);
        button2 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_2);

        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                present(new AutoProgressSlice(), new Intent());
            }
        });

        button2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                present(new ManuaProgressSlice(), new Intent());
            }
        });

        super.setUIContent(rootLayout);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
