## DownloadProgressBar

## Introduction
DownloadProgressBar is a library that delivers awesome custom progress bar. You can manipulate it's state in every way.

## Usage instructions
Below will show You how to use this custom view. To play success animation, simply call this one line:

DownloadProgressBar downloadProgressBar = (DownloadProgressBar)findViewById(R.id.download_progress_view);
downloadProgressBar.playToSuccess();
Also added listener for common events: whole animation start, whole animation end, progress update, animation success, animation error. To define it, call this one:

downloadProgressBar.setOnProgressUpdateListener(new DownloadProgressBar.OnProgressUpdateListener() {
            @Override
            public void onProgressUpdate(float currentPlayTime) {
                // Here we are setting % value on our text view.
                successTextView.setText(Math.round(currentPlayTime / 3.6) + " %");
            }

            @Override
            public void onStarted() {
                // Here we are disabling our view because of possible interactions while animating.
                downloadProgressBar.setEnabled(false);
            }

            @Override
            public void onEnded() {
                successTextView.setText("Click to download");
                downloadProgressBar.setEnabled(true);
            }

            @Override
            public void onAnimationSuccess() {
                successTextView.setText("Downloaded!");
            }

            @Override
            public void onAnimationError() {

            }
        });
        
Ordinary use:
``` xml
     <com.panwrona.downloadprogressbar.library.DownloadProgressBar
        ohos:id="$+id:dpv3"
        ohos:width="150vp"
        ohos:height="150vp"
        app:circleBackgroundColor="$color:red_light_primary_color"
        app:progressBackgroundColor= "$color:red_light_primary_color"
        app:progressColor= "$color:red_text_icon_color"
        app:drawingColor= "$color:red_text_icon_color"
        app:progressDuration="2000"
        app:resultDuration="5000"
        app:circleRadius="70vp"
        app:lineWidth="6vp"
        app:strokeWidth="6vp"
        app:overshootValue="2.5"
        />
```


## Installation instruction
**Method 1:**
Generate the .har package through the library and add the .har package to the libs folder.
Add the following code to the entry gradle:
```
implementation fileTree  (dir: 'libs', include: ['*.jar', '*.har'])
```
**Method 2:**
In project level build.gradle:
```
allprojects{
    repositories{
        mavenCentral()
    }
}
```
Add the following code to the entry gradle:
```
implementation project(path: ': DownloadProgressBar)
```
**Method 3:**
For using DownloadProgressBar from the remote repository, add the below dependency in "entry" build.gradle.

    Modify entry build.gradle as below :
    dependencies {
        implementation 'io.openharmony.tpc.thirdlib:DownloadProgressBar:1.0.0'
    }

## License 
``` 

Copyright 2015 Mariusz Brona

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.